#!/bin/sh

case "$1" in
  "dev-django")
    if [ -f "/usr/bin/python3" ]; then
      /usr/bin/python3 manage.py runserver 0.0.0.0:8080
    else
      /usr/bin/python manage.py runserver 0.0.0.0:8080
    fi
    ;;
  "dev-worker")
    /usr/local/bin/watchmedo auto-restart --directory=./ --pattern=*.py --recursive -- /usr/local/bin/celery worker --app=django_rest_pg.celery.app --concurrency=1 --loglevel=INFO
    ;;
  "dev-beat")
    /usr/local/bin/watchmedo auto-restart --directory=./ --pattern=*.py --recursive -- /usr/local/bin/celery beat --app=django_rest_pg.celery.app --scheduler django_celery_beat.schedulers:DatabaseScheduler --loglevel=INFO
    ;;
  "prod-django")
    /usr/local/bin/gunicorn django_rest_pg.wsgi:application --bind 0.0.0.0:8081
    ;;
  "prod-worker")
    /usr/local/bin/celery worker --backend=django_rest_pg.celery.backend --concurrency=1 --loglevel=INFO
    ;;
  "prod-beat")
    /usr/local/bin/celery beat --backend=django_rest_pg.celery.backend --scheduler django_celery_beat.schedulers:DatabaseScheduler --loglevel=INFO
    ;;
  *)
    echo "Running the command '$@'..."
    exec "$@"
    ;;
esac
