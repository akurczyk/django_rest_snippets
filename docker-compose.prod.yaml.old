version: '3.7'

services:

  nginx:
    build:
      context: ./nginx
      dockerfile: nginx/Dockerfile
    ports:
      - '8080:8080'
    volumes:
      - django_static_data:/usr/share/nginx/html/static
      - django_media_data:/usr/share/nginx/html/media
    depends_on:
      - backend

  backend:
    build:
      context: ./backend
      dockerfile: backend/Dockerfile
    image: backend
    command: prod-django
    ports:
      - '8081:8081'
    volumes:
      - django_static_data:/backend/static
      - django_media_data:/backend/media
    env_file: .env
    depends_on:
      - postgres
      - rabbitmq

  postgres:
    image: postgres:alpine
    ports:
      - '5432:5432'
    volumes:
      - postgres_data:/var/lib/postgresql/data
    env_file: .env

  rabbitmq:
    image: rabbitmq:alpine
    volumes:
      - rabbitmq_data:/var/lib/rabbitmq
    env_file: .env

  celery_worker:
    image: backend
    command: prod-worker
    volumes:
      - django_static_data:/backend/static
      - django_media_data:/backend/media
    env_file: .env
    depends_on:
      - backend

  celery_beat:
    image: backend
    command: prod-beat
    volumes:
      - django_static_data:/backend/static
      - django_media_data:/backend/media
    env_file: .env
    depends_on:
      - backend

volumes:

  django_static_data:
  django_media_data:
  postgres_data:
  rabbitmq_data:
